import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';


import { AppComponent } from './app.component';
import { AchatComponent } from './achat/achat.component';
import { ContactComponent } from './contact/contact.component';
import { GerantComponent } from './gerant/gerant.component';
import { CommandeComponent } from './commande/commande.component';
import { IndexComponent } from './index/index.component';
import { FooterComponent } from './footer/footer.component';
import { CommandeFormuleComponent } from './commande-formule/commande-formule.component';
import { MagasinerComponent } from './magasiner/magasiner.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PaimentComponent } from './paiment/paiment.component';

@NgModule({
  declarations: [
    AppComponent,
    AchatComponent,
    ContactComponent,
    GerantComponent,
    CommandeComponent,
    IndexComponent,
    FooterComponent,
    CommandeFormuleComponent,
    MagasinerComponent,
    PaimentComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule
    


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
