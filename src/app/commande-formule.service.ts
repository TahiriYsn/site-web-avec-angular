import { Injectable } from '@angular/core';
import { articles } from '../assets/data/commandes.json'


@Injectable({
  providedIn: 'root'
})
export class CommandeFormuleService {
  
  private art: any[] = articles;
  constructor() { }
  afficheListee: boolean = false; 



  getArticle() {
    return this.art;
  }
 
  affichee() {
    this.afficheListee = true;
  }
    

}
