import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as listeArticle from '../assets/data/bd.json';
import * as listeArticles from '../assets/data/commandes.json'
import { CommandeFormuleService } from './commande-formule.service';
import { AppRoutingModule } from './app-routing.module';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'qi';
  article:any = (listeArticle as any).default;
  afficheListe: boolean = false; 
  
  afficheListee: boolean = false; 
  art: any = (listeArticles as any).default;
  constructor(private commandeformuleService : CommandeFormuleService) { }
    
  affiche() {
    this.afficheListe = true;
  }
    
  cacher() {
    this.afficheListe = false;
  }

  getTous() :any [] {
    return this.commandeformuleService.getArticle();
  }
  affichee() {
    this.afficheListee = true;
  }

}
