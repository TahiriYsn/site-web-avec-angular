import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MagasinerComponent } from './magasiner/magasiner.component';
import { PaimentComponent } from './paiment/paiment.component';
import { ContactComponent } from './contact/contact.component';
import { CommandeComponent } from './commande/commande.component';
import { GerantComponent } from './gerant/gerant.component';


const Routes: Routes = [
    { path: 'magasiner', component: MagasinerComponent },
    { path: 'paiment', component: PaimentComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'commande', component: CommandeComponent },
    { path: 'gerant', component: GerantComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(Routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }