import { TestBed } from '@angular/core/testing';

import { CommandeFormuleService } from './commande-formule.service';

describe('CommandeFormuleService', () => {
  let service: CommandeFormuleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommandeFormuleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
