import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommandeFormuleComponent } from './commande-formule.component';

describe('CommandeFormuleComponent', () => {
  let component: CommandeFormuleComponent;
  let fixture: ComponentFixture<CommandeFormuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommandeFormuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommandeFormuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
